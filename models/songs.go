package models

import (
	"fmt"
	"github.com/astaxie/beego/orm"
)

type Songs struct {
	Id              int    `orm:"column(id);pk"`
	Name            string `orm:"column(name);size(255);null"`
	SongLyric       string `orm:"column(song_lyric);size(255);null"`
	Image           string `orm:"column(image);size(255);null"`
	CompositionDate string `orm:"column(composition_date);size(255);null"`
	KindID          *Kinds `orm:"column(kindID);rel(fk)"`
	CreatedAt       string `orm:"column(created_at);size(255);null"`
	UpdatedAt       string `orm:"column(updated_at);size(255);null"`
	AdminID         int    `orm:"column(adminID)"`
	Flag            int    `orm:"column(flag)"`
}

func (t *Songs) TableName() string {
	return "songs"
}

func init() {
	orm.RegisterModel(new(Songs))
}

// AddSongs insert a new Songs into database and returns
// last inserted Id on success.
func AddSongs(m *Songs) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetSongsById retrieves Songs by Id. Returns error if
// Id doesn't exist
func GetSongsById(id int) (v *Songs, err error) {
	o := orm.NewOrm()
	v = &Songs{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllSongs retrieves all Songs matches certain condition. Returns empty list if
// no records exist
func GetAllSongs(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	
	return nil, err
}

// UpdateSongs updates Songs by Id and returns error if
// the record to be updated doesn't exist
func UpdateSongsById(m *Songs) (err error) {
	o := orm.NewOrm()
	v := Songs{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeleteSongs deletes Songs by Id and returns error if
// the record to be deleted doesn't exist
func DeleteSongs(id int) (err error) {
	o := orm.NewOrm()
	v := Songs{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&Songs{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
